---
title: About me
subtitle: Why you'd want to hang out with me
comments: false
---

Ik ben Michelle, ik studeer momenteel Communication & Multimedia Design aan de Hogeschool Rotterdam. Ik zit nu in mijn eerste jaar.

### Mijn goals voor dit jaar

- Beter worden in programmeren (java/html/css/markdown, etc)
- Mijn skills in photoshop en illustrator verder ontwikkelen
- Een ontwerpproces vinden dat goed bij mij past

